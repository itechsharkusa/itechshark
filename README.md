iTechshark in St. Charles is a one stop shop for all things Apple. Professional repair and service for iPad, iPhone, Mac, and iPod. We specialize in fixing your device the correct way in a timely matter. We also sell pre-owned apple devices. Lowest price guaranteed. We warranty all devices.

Address: 494 S 5th St, St Charles, MO 63301, USA

Phone: 636-493-5053
